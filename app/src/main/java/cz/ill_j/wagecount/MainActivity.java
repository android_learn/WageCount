package cz.ill_j.wagecount;

import android.app.WallpaperManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

//    try {
//      WallpaperManager wpm = WallpaperManager.getInstance(context);
//      InputStream ins = null;
//      ins = new URL("res/drawable/").openStream();
//      wpm.setStream(ins);
//
//    } catch (IOException e) {
//      e.printStackTrace();
//    }
  }

  public void goToSource(View view) {
    Intent intent = new Intent(MainActivity.this, SourceActivity.class);
    startActivity(intent);
  }

}
