package cz.ill_j.wagecount;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Pick up all sources from input form and process calculation.
 */
public class SourceActivity extends AppCompatActivity {

  Button btnAdd;
  Button btnReset;

  TextView grossWage;
  TextView noOfChildren;
  int intChildren = 0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_source);

    btnAdd = (Button)findViewById(R.id.buttonAdd);
    btnReset = (Button)findViewById(R.id.buttonZero);

    noOfChildren = (TextView)findViewById(R.id.textChildren);

    btnAdd.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        String children = noOfChildren.getText().toString();

        intChildren = Integer.parseInt(children);
        intChildren++;

        noOfChildren.setText(String.valueOf(intChildren));
      }
    });

    btnReset.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        noOfChildren.setText(String.valueOf(0));
        intChildren = 0;
      }
    });

  }


  /**
   * Covers more processes, calculation, saving etc.
   * @param view
     */
  public void processCalculation(View view) {

    CountWage countWage = new CountWage();
    grossWage  = (TextView)findViewById(R.id.editNumber);
    int source = Integer.parseInt(grossWage.getText().toString());
    BigDecimal grossWageFromGui = new BigDecimal(source);

    // calculation and Wage object fill-in
    Wage wage = new Wage();
    wage = countWage.getFinalWage(grossWageFromGui, intChildren);

    //TODO: save to DB
    // TODO: checkbox if I want to save record
    // boolean isStored = saveNewRecord(jsonEngine.objectToJsonString(wage);)
    DatabaseHandler databaseHandler = new DatabaseHandler(this);
    databaseHandler.saveNewRecord(wage);

    //TODO: alert on screen in result activity

    // pass data to next activity
    Intent intent = new Intent(SourceActivity.this, ResultActivity.class);
    intent.putExtra("FINAL_WAGE", (Serializable) wage);
    startActivity(intent);

    // check DB content

    System.out.println();
  }

  // go to DB screen
  public void goToDb (View view) {
    Intent intent = new Intent(SourceActivity.this, ListView_db.class);
    startActivity(intent);
  }

}
