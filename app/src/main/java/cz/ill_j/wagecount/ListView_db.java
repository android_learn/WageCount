package cz.ill_j.wagecount;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ill_j on 16.7.2017.
 */
public class ListView_db extends Activity {

  @Override
  public void onCreate (Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_wagerecordslist);

    final ListView listview = (ListView) findViewById(R.id.listview);

    DatabaseHandler databaseHandler = new DatabaseHandler(this);
    List<Wage> wageObjectList = databaseHandler.getAllWageRecords();
    List<String> wageLabelList = new ArrayList<>();
    for (Wage wage : wageObjectList) {
      wageLabelList.add(wage.getGrossWage().toString());
    }


//    final StableArrayAdapter adapter = new StableArrayAdapter(this,
//      android.R.layout.simple_list_item_1, wageLabelList);
//    listview.setAdapter(adapter);



    // pass object data to another activity, depends on current item
    listview.setOnItemClickListener(
      new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

          Object wage = listview.getItemAtPosition(position);


          // pass data to next activity
          Intent intent = new Intent(ListView_db.this, ResultActivity.class);
          intent.putExtra("FINAL_WAGE", (Serializable) wage);
          startActivity(intent);
        }
      }
    );

  }

  // handling adapter


}
