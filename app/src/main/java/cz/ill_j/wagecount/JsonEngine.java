package cz.ill_j.wagecount;

import com.google.gson.Gson;

/**
 * Created by ill_j on 11.7.2017.
 */
public class JsonEngine {

  private Gson gson = new Gson();

  /**
   *
   * @param obj
   * @return json object in String
     */
  public String objectToJsonString (Object obj) {
    String jsonInString = gson.toJson(obj);
    return jsonInString;
  }

  /**
   *
   * @param jsonInString
   * @return Wage object
     */
  public Wage jsonToWage (String jsonInString) {
    Wage wage = gson.fromJson(jsonInString, Wage.class);
    return wage;
  }

}
