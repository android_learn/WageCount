package cz.ill_j.wagecount;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by ill_j on 26.7.2017.
 */
public class WageAdapter extends BaseAdapter {

  private Context mContext;
  private LayoutInflater mInflater;
  private List<Wage> mDataSource;

  // constructor
  public WageAdapter (Context context, List<Wage> items) {
    mContext = context;
    mDataSource = items;
    mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Override
  public int getCount() {
    return mDataSource.size();
  }

  @Override
  public Object getItem(int position) {
    return mDataSource.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int i, View view, ViewGroup parent) {

    // Get view for row item
    View rowView = mInflater.inflate(R.layout.activity_wagerecordslist, parent, false);
    return rowView;
  }
}
