package cz.ill_j.wagecount;

import java.math.BigDecimal;

/**
 * Created by ill_j on 23.2.2017.
 * description
 * Enum of CONSTANTS used in wage counting
 */
public enum Constants {

  BASETAXRATE(BigDecimal.valueOf(0.15)),
  EMPLOYEEDISCOUNT(BigDecimal.valueOf(2070)),
  INSURANCEHEALTHRATE(BigDecimal.valueOf(0.045)),
  INSURANCESOCIALRATE(BigDecimal.valueOf(0.065)),
  // rate to count super gross wage = 9% + 25%
  SUPERRATE(BigDecimal.valueOf(1.34)),
  // tax discount
  FIRSTCHILD(BigDecimal.valueOf(1117)),
  SECONDCHILD(BigDecimal.valueOf(1617)),
  NEXTCHILD(BigDecimal.valueOf(2017))
  ;

  private BigDecimal value;

  private Constants(BigDecimal value) {
    this.value = value;
  }
  public BigDecimal getValue() {
    return value;
  }

}
