package cz.ill_j.wagecount;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ill_j on 23.2.2017.
 */
public class CountWage {


  /**
   * Count mechanism which fill-in Wage bean.
   *
   * param grossWage
   * param noOfChildren
   * param partTimeJob
   * param typeOfDisability
   * param isStudent
   * return
   * 			final wage, which you should get on your bank account
   */
//	private Wage getFinalWage(BigDecimal grossWage, int noOfChildren, int partTimeJob, int typeOfDisability, boolean isStudent) {
  public Wage getFinalWage(BigDecimal grossWage, int noOfChildren) {

    Wage wage = new Wage();

    // time setting in Wage bean
    SupportMethods supportMethods = new SupportMethods();
    String date = supportMethods.getFormatedDate(new Date());
    wage.setDate(date);

    // set wage inputs
    wage.setGrossWage(grossWage);
    wage.setNoOfChildren(noOfChildren);


    BigDecimal taxBase;

    if ((grossWage.compareTo(BigDecimal.ZERO)) == -1 || (grossWage.compareTo(BigDecimal.ZERO)) == 0) {
      return wage; // TODO: error message
    }

    if (noOfChildren > 0) {
      wage.setTaxBenefitChildren(getTaxBenefitChildren(noOfChildren));
    }

    // base of tax, % of super gross wage we have to round
    taxBase = (getSuperGrossWage(grossWage).multiply(Constants.BASETAXRATE.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP)).subtract(Constants.EMPLOYEEDISCOUNT.getValue()).subtract(wage.getTaxBenefitChildren());
    wage.setSuperGrossWage(getSuperGrossWage(grossWage));
    wage.setTaxBase(taxBase);

    wage.setInsuranceEmployee(getInsuranceEmployee(grossWage));
    wage.setInsuranceHealth(getInsuranceHealth(grossWage));
    wage.setInsuranceSocial(getInsuranceSocial(grossWage));
    wage.setResult(grossWage.subtract(taxBase).subtract(wage.getInsuranceEmployee()));

    return wage;
  }

  /**
   *
   * @param grossWage
   * @return
   * 			money you have to sent to state institutions, health and social insurance
   */
  public BigDecimal getInsuranceEmployee(BigDecimal grossWage) {
    BigDecimal insuranceHealth = grossWage.multiply(Constants.INSURANCEHEALTHRATE.getValue());
    BigDecimal insuranceSocial = grossWage.multiply(Constants.INSURANCESOCIALRATE.getValue());

    System.out.println("Zdravotni pojisteni zamestnance " + insuranceHealth);
    System.out.println("Socialni pojisteni zamestnance " + insuranceSocial);

    return insuranceHealth.add(insuranceSocial);
  }

  private BigDecimal getInsuranceHealth(BigDecimal grossWage) {
    return grossWage.multiply(Constants.INSURANCEHEALTHRATE.getValue());
  }

  private BigDecimal getInsuranceSocial(BigDecimal grossWage) {
    return grossWage.multiply(Constants.INSURANCESOCIALRATE.getValue());
  }

  /**
   *
   * @param grossWage
   * @return
   * 			czech special issue, SUPER gross wage
   */
  public BigDecimal getSuperGrossWage(BigDecimal grossWage) {
    BigDecimal superGrossWage = (grossWage.multiply(Constants.SUPERRATE.getValue()));

    System.out.println("Superhruba mzda " + superGrossWage);
    return superGrossWage;
  }

  /**
   *
   * @param noOfChildren
   * @return
   * 			tax discount for all children together
   */
  private BigDecimal getTaxBenefitChildren(int noOfChildren) {
    BigDecimal taxBenefitChildren = BigDecimal.ZERO;
    switch (noOfChildren) {
      case 1:
        taxBenefitChildren = Constants.FIRSTCHILD.getValue();
        break;

      case 2:
        taxBenefitChildren = Constants.FIRSTCHILD.getValue().add(Constants.SECONDCHILD.getValue());
        break;

      default:
        // addition constants, 1st and 2nd child, adding multiplication for each next child
        // FIRSTCHILD + SECONDCHILD + (NEXTCHILD * (noOfChildren - 2))
        taxBenefitChildren = Constants.FIRSTCHILD.getValue().add(Constants.SECONDCHILD.getValue()).add((BigDecimal.valueOf(noOfChildren).subtract(BigDecimal.valueOf(2)).multiply(Constants.NEXTCHILD.getValue())));
        break;
    }

    return taxBenefitChildren;
  }


}
