package cz.ill_j.wagecount;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ill_j on 24.7.2017.
 */
public class SupportMethods {

  // Time handling

  private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

  public String getFormatedDate (Date date) {
    return dateFormat.format(date);
  }
}
