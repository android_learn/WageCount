package cz.ill_j.wagecount;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by ill_j on 23.2.2017.
 */
public class ResultActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_result);

    Wage wage = new Wage();
    wage = (Wage)getIntent().getSerializableExtra("FINAL_WAGE");

    TextView resultGrossWage = (TextView) findViewById(R.id.grossWage);
    TextView resultSuperGrossWage = (TextView) findViewById(R.id.superGrossWage);
    TextView resultInsuranceHealth = (TextView) findViewById(R.id.insuranceHealth);
    TextView resultInsuranceSocial = (TextView) findViewById(R.id.insuranceSocial);
    TextView resultChildrenNo = (TextView) findViewById(R.id.childrenNo);
    TextView resultChildrenDiscount = (TextView) findViewById(R.id.childrenDiscount);

    TextView resultNetto = (TextView) findViewById(R.id.resultNetto);
    TextView resultTaxBase = (TextView) findViewById(R.id.taxBase);

    resultGrossWage.setText(wage.getGrossWage().toString());
    resultSuperGrossWage.setText(wage.getSuperGrossWage().toString());
    resultNetto.setText(wage.getResult().toString());
    resultTaxBase.setText(wage.getTaxBase().toString());
    resultInsuranceHealth.setText(wage.getInsuranceHealth().toString());
    resultInsuranceSocial.setText(wage.getInsuranceSocial().toString());
    resultChildrenNo.setText(Integer.toString(wage.getNoOfChildren()));
    resultChildrenDiscount.setText(wage.getTaxBenefitChildren().toString());

    // DB records count
    TextView resultDbRecordsCount = (TextView) findViewById(R.id.dbRecordsCount);
    DatabaseHandler databaseHandler = new DatabaseHandler(this);
    resultDbRecordsCount.setText(Integer.toString(databaseHandler.getWagesCount()));
  }

  public void exitActivity(View view) {
    finish();
  }

}
