package cz.ill_j.wagecount;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ill_j on 11.7.2017. <br>
 * Handles all DB requests. <br>
 * Uses SQLite. <br>
 */
public class DatabaseHandler extends SQLiteOpenHelper {

  // All Static variables
  // Database Version
  private static final int DATABASE_VERSION = 1;

  // Database Name
  private static final String DATABASE_NAME = "spyWages";

  // Wages table name
  private static final String TABLE_WAGES = "wagesTable";

  // Wages Table Columns names
  private static final String KEY_ID = "id";
  private static final String KEY_TIME = "time";
  private static final String KEY_WAGE_OBJECT = "wageBlob";  // will be used blob type

  /**
   * Constructor
   * @param context
     */
  public DatabaseHandler(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  // Creating Tables
  @Override
  public void onCreate(SQLiteDatabase db) {
    String CREATE_WAGES_TABLE = "CREATE TABLE " + TABLE_WAGES + "("
      + KEY_ID + " INTEGER PRIMARY KEY autoincrement,"
      //+ KEY_TIME + " TEXT,"
      + KEY_WAGE_OBJECT + " TEXT" + ")";  // BLOB not null
    db.execSQL(CREATE_WAGES_TABLE);
  }

  // Upgrading database
  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // Drop older table if existed
    db.execSQL("DROP TABLE IF EXISTS " + TABLE_WAGES);

    // Create tables again
    onCreate(db);
  }

  //////////////////////////////////////////////////////////////////////////
  // ⇒All CRUD Operations
  //////////////////////////////////////////////////////////////////////////

  /**
   * Save new record of Wage object to SQLite
   * @param wage
     */
  public void saveNewRecord (Wage wage) {
    SQLiteDatabase db = this.getWritableDatabase();

    // create row
    JsonEngine jsonEngine = new JsonEngine();

    ContentValues values = new ContentValues();
    values.put(KEY_WAGE_OBJECT, jsonEngine.objectToJsonString(wage));

    // Inserting Row
    try {
      long id = db.insert(TABLE_WAGES, null, values);
      if (id == -1) {
        throw new RuntimeException("Zapis do DB se nezdaril: " + " | GrossWage " + wage.getGrossWage() + " | Counted at " + wage.getDate());
      }
      Log.i("DatabaseHandler","Insert probehl v poradku.");
    } catch (RuntimeException ex) {
        Log.e("DatabaseHandler","Odchycena vyjimka pri pokusu insertnout novy zaznam.",ex);
    } finally {
      db.close(); // Closing database connection
    }
  }

  /**
   *
   * @return
     */
  public List<Wage> getAllWageRecords () {

    JsonEngine jsonEngine = new JsonEngine();
    List<Wage> wageList = new ArrayList<>();
    String selectQuery = "SELECT  * FROM " + TABLE_WAGES;

    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);

    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
      do {
        Wage wage = new Wage();
        wage = jsonEngine.jsonToWage(cursor.getString(1));
        wage.setDbId(Integer.parseInt(cursor.getString(0)));  //TODO: handle ID
        // Adding wage to list
        wageList.add(wage);
      } while (cursor.moveToNext());
    }
    return wageList;
  }

  public List<String> getAllWageRecordsInString () {

    JsonEngine jsonEngine = new JsonEngine();
    List<String> wageList = new ArrayList<>();
    String selectQuery = "SELECT  * FROM " + TABLE_WAGES;

    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);

    // looping through all rows and adding to list
    if (cursor.moveToFirst()) {
      do {
        String wage;
//        wage.setID(Integer.parseInt(cursor.getString(0)));  //TODO: handle ID
        wage = cursor.getString(1);
        // Adding wage to list
        wageList.add(wage);
      } while (cursor.moveToNext());
    }
    return wageList;
  }


  // Getting records Count
  public int getWagesCount() {
    String countQuery = "SELECT  * FROM " + TABLE_WAGES;
    SQLiteDatabase db = this.getReadableDatabase();
    Cursor cursor = db.rawQuery(countQuery, null);
    int count = cursor.getCount();
    cursor.close();

    // return count
    return count;
  }

}
