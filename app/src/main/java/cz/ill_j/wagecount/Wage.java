package cz.ill_j.wagecount;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ill_j on 23.2.2017.
 * bean
 */
public class Wage implements Serializable {

  // identification
  private int dbId;

  // inputs
  private int noOfChildren;
  int partTimeJob;
  int typeOfDisability;
  boolean isStudent;

  // outputs
  BigDecimal result = BigDecimal.ZERO;
  BigDecimal taxBenefitChildren = BigDecimal.ZERO;
  BigDecimal taxBase;
  BigDecimal insuranceEmployee = BigDecimal.ZERO;
  BigDecimal superGrossWage;
  BigDecimal insuranceHealth;
  BigDecimal insuranceSocial;

  // Time
  private String date;

  /**
   *
   * @return gross wage in BigDecimal format.
     */
  public BigDecimal getGrossWage() {
    return grossWage;
  }

  public void setGrossWage(BigDecimal grossWage) {
    this.grossWage = grossWage;
  }

  private BigDecimal grossWage;

  public boolean isStudent() {
    return isStudent;
  }

  public void setStudent(boolean student) {
    isStudent = student;
  }

  public int getNoOfChildren() {
    return noOfChildren;
  }

  public void setNoOfChildren(int noOfChildren) {
    this.noOfChildren = noOfChildren;
  }
  /*
   */
  public BigDecimal getResult() {
    return result;
  }

  public void setResult(BigDecimal result) {
    this.result = result;
  }

  public BigDecimal getTaxBenefitChildren() {
    return taxBenefitChildren;
  }

  public void setTaxBenefitChildren(BigDecimal taxBenefitChildren) {
    this.taxBenefitChildren = taxBenefitChildren;
  }

  public BigDecimal getTaxBase() {
    return taxBase;
  }

  public void setTaxBase(BigDecimal taxBase) {
    this.taxBase = taxBase;
  }

  public BigDecimal getInsuranceEmployee() {
    return insuranceEmployee;
  }

  public void setInsuranceEmployee(BigDecimal insuranceEmployee) {
    this.insuranceEmployee = insuranceEmployee;
  }

  public BigDecimal getSuperGrossWage() {
    return superGrossWage;
  }

  public void setSuperGrossWage(BigDecimal superGrossWage) {
    this.superGrossWage = superGrossWage;
  }

  public BigDecimal getInsuranceHealth() {
    return insuranceHealth;
  }

  public void setInsuranceHealth(BigDecimal insuranceHealth) {
    this.insuranceHealth = insuranceHealth;
  }

  public BigDecimal getInsuranceSocial() {
    return insuranceSocial;
  }

  public void setInsuranceSocial(BigDecimal insuranceSocial) {
    this.insuranceSocial = insuranceSocial;
  }


  // Time handling

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public int getDbId() {
    return dbId;
  }

  public void setDbId(int dbId) {
    this.dbId = dbId;
  }

}
